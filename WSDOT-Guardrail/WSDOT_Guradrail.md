# Washington State DOT

WSDOT publishes their data in an open data portal. We specifically want to use their guardail points and guradrail lines data

https://geo.wa.gov/datasets/WSDOT::wsdot-guardrail-points/explore
https://geo.wa.gov/datasets/WSDOT::wsdot-guardrail-lines/explore

Navigating the menus eventaully yields a link to a GEOJSON of the data

https://opendata.arcgis.com/datasets/138e694acf5846c2998b46c05ca54681_1.geojson
https://opendata.arcgis.com/datasets/cfa2ddbc70ca4816938afaf797d0122b_2.geojson