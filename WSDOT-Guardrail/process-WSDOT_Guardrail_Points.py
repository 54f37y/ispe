"""
Converts the WSDOT GEOJSONs into GEOJSONs ready for map.safe7y.com

"""

# Imports ----------------------------------------------------------------------
import time
import pandas as pd
import os
import json


# Settings ---------------------------------------------------------------------
verbose = True

pd.set_option('display.max_columns', None)

# Input ------------------------------------------------------------------------
def main():
    # time the operation - if verbose
    if verbose:
        start = time.time()

    # import the JSON as a dataframe
    with open("data/WSDOT_guardrail_points.json", "r") as file:
        data = file.read()
        json_data = json.loads(data)
        print(json_data['features'][0])
        df = pd.DataFrame.from_dict(pd.json_normalize(json_data['features']), orient='columns')

    # save only the columns we need
    df = df[['properties.MaintenanceAreaName', 'properties.CategoryType', 
             'properties.LRSYear', 'properties.EndTreatmentType', 
             'geometry.coordinates'
            ]]

    # create the FeatureType column
    feature_mapping = {'Softstop':'Terminal',
                  'Anchors':'Terminal',
                  'Connection to wall, bridge rail, or other barrier':'VehicleBarrier',
                  'SKT':'Terminal',
                  'BCT':'Terminal',
                  'MSKT':'Terminal',
                  'ET-Plus/ET-31':'Terminal',
                  'SRT':'Terminal',
                  'Buried-In-Backslope':'Terminal',
                  'FLEAT':'Terminal',
                  'MELT':'Terminal',
                  'ET-2000':'Terminal',
                  'Connection to wall, bridge rail or other barrier':'VehicleBarrier',
                  'Other':'Terminal',
                  'Bullnose':'Terminal',
                  'BEAT':'Terminal',
                  'ELT':'Terminal',
                  'SoftStop®':'Terminal',
                  None: 'Terminal',
                  }
    df['FeatureType'] =  df['properties.EndTreatmentType'].map(feature_mapping)

    # create the SubType column
    subtype_mapping = {'Softstop':'EndTerminal',
                  'Anchors':'EndTerminal',
                  'Connection to wall, bridge rail, or other barrier':'Transition',
                  'SKT':'EndTerminal',
                  'BCT':'EndTerminal',
                  'MSKT':'EndTerminal',
                  'ET-Plus/ET-31':'EndTerminal',
                  'SRT':'EndTerminal',
                  'Buried-In-Backslope':'EndTerminal',
                  'FLEAT':'EndTerminal',
                  'MELT':'EndTerminal',
                  'ET-2000':'EndTerminal',
                  'Connection to wall, bridge rail or other barrier':'Transition',
                  'Other':'EndTerminal',
                  'Bullnose':'CrashCushion',
                  'BEAT':'EndTerminal',
                  'ELT':'EndTerminal',
                  'SoftStop®':'EndTerminal',
                  None: 'EndTerminal',
                  }
    df['Subtype'] =  df['properties.EndTreatmentType'].map(subtype_mapping)

    # create the Device column
    device_mapping = {'Softstop':'Soft Stop End Terminal',
                  'Anchors':'Trailing-End Anchorage',
                  'Connection to wall, bridge rail, or other barrier':None,
                  'SKT':'Sequential Kinking Terminal (SKT)',
                  'BCT':'Breakaway Cable Terminal (BCT)',
                  'MSKT':'MASH Sequential Kinking Terminal (MSKT)',
                  'ET-Plus/ET-31':'Extruder Terminal (ET-Plus)',
                  'SRT':'Slotted Rail Terminal (SRT) 350',
                  'Buried-In-Backslope':'Buried-in-Backslope Terminal',
                  'FLEAT':'Flared Energy-Absorbing Terminal (FLEAT)',
                  'MELT':'Modified Eccentric Loader Terminal (MELT)',
                  'ET-2000':'Extruder Terminal (ET-Plus)',
                  'Connection to wall, bridge rail or other barrier':None, 
                  'Other':None,
                  'Bullnose':'Thrie-Beam Bullnose Guardrail System',
                  'BEAT':'Bursting Energy Absorbing Terminal (BEAT)',
                  'ELT':'Eccentric Loader Terminal (ELT)',
                  'SoftStop®':'Soft Stop End Terminal',
                  None: None,
                  }
    df['Device'] =  df['properties.EndTreatmentType'].map(device_mapping)

    # create the Geometry column
    geometry_mapping = {'Softstop':'Point',
                        'Anchors':'Point',
                        'Connection to wall, bridge rail, or other barrier':'LineString',
                        'SKT':'Point',
                        'BCT':'Point',
                        'MSKT':'Point',
                        'ET-Plus/ET-31':'Point',
                        'SRT':'Point',
                        'Buried-In-Backslope':'Point',
                        'FLEAT':'Point',
                        'MELT':'Point',
                        'ET-2000':'Point',
                        'Connection to wall, bridge rail or other barrier':'LineString',
                        'Other':'Point',
                        'Bullnose':'Point',
                        'BEAT':'Point',
                        'ELT':'Point',
                        'SoftStop®':'Point',
                        None: 'Point',
                        }
    df['Geometry'] =  df['properties.EndTreatmentType'].map(geometry_mapping)
    df['latitude'] = df['geometry.coordinates']
    df['longitude'] = df['geometry.coordinates']

    # create the notes column
    df['Notes'] = 'Maintenance Area: ' + df['properties.MaintenanceAreaName'] \
                  + ' and Category: ' + df['properties.CategoryType']

    # create the wikiUrl column
    wiki_mapping = {'Softstop':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/SoftStop',
                    'Anchors':'https://wiki.safe7y.com/en/Features/Terminal/EndTerminal/TrailingEndAnchor',
                    'Connection to wall, bridge rail, or other barrier':'https://wiki.safe7y.com/Features/VehicleBarrier/Transition',
                    'SKT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/SKT',
                    'BCT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/BCT',
                    'MSKT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/SKT',
                    'ET-Plus/ET-31':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/ETPlus',
                    'SRT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/SRT',
                    'Buried-In-Backslope':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/Buried',
                    'FLEAT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/FLEAT',
                    'MELT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/MELT',
                    'ET-2000':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/ETPlus',
                    'Connection to wall, bridge rail or other barrier':'https://wiki.safe7y.com/Features/VehicleBarrier/Transition',
                    'Other':'https://wiki.safe7y.com/en/Features/Terminal/EndTerminal',
                    'Bullnose':'https://wiki.safe7y.com/Features/Terminal/CrashCushion',
                    'BEAT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/BEAT',
                    'ELT':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/ELT',
                    'SoftStop®':'https://wiki.safe7y.com/Features/Terminal/EndTerminal/SoftStop',
                    None: 'Point',
                    }
    df['WikUrl'] =  df['properties.EndTreatmentType'].map(wiki_mapping)

    # add some map.safe7y.com specific data fields
    df['Source'] = 'Washington State DOT'
   
    # rename the coordinate columns
    df.rename(columns={"geometry.coordinates": "coordinates"})

    # drop the uneccesary columns
    df = df.drop(columns=['properties.MaintenanceAreaName', 'properties.CategoryType', 
                          'properties.LRSYear', 'properties.EndTreatmentType'
                         ])

    # print some stats on the file - if verbose
    if verbose:
        print(df.dtypes)
        print(df)

    return()

    # save the data frame to a json
    # df.to_json(output_file, orient='index', indent=2)
    properties = ['FeatureType', 'Subtype', 'Device', 'Source', 'WikiLink', 'TotalFatalities']
    json_df = df_to_geojson(df, properties)

    return

    # save the json
    with open(output_file, 'w', encoding='utf-8') as f:
        json.dump(json_df, f, ensure_ascii=False, indent=2)

    # report completion and time - if verbose
    if args.verbose:
        print("DONE - completed in " + str(time.time() - start) + " seconds")


def df_to_geojson(df, properties, lat='latitude', lon='longitude'):
    """ Converts a pandas dataframe to GEOJSON. Thanks to Geoff Boeing for this function.
        https://geoffboeing.com/2015/10/exporting-python-data-geojson/
        df -- dataframe to convert
        properties -- list of column name strings to convert into properties
        lat -- column name string of the Latitude column
        lon -- column name string of the Longitude column
    """
    geojson = {'type': 'FeatureCollection', 'features': []}
    for _, row in df.iterrows():
        feature = {'type': 'Feature',
                   'properties': {},
                   'geometry': {'type': 'Point',
                                'coordinates': []}}
        feature['geometry']['coordinates'] = [row[lon], row[lat]]
        for prop in properties:
            feature['properties'][prop] = row[prop]
        geojson['features'].append(feature)
    return geojson

if __name__ == '__main__':
    main()
