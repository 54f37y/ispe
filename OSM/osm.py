""" osm.py
This script downloads all the instances of the desired elements to a GEOJSON.
"""

# imports
from OSMPythonTools.nominatim import Nominatim
from OSMPythonTools.overpass import overpassQueryBuilder
from OSMPythonTools.overpass import Overpass
from datetime import datetime
import json
import os

# settings
verbose = True
area = 'United States'  # the area you want to search in
element_type = 'way'
output_folder = './data/OSM/'
time_out = 240  # allowable query time in seconds
source = 'OpenStreetMap - ' + str(datetime.now())
# a dict with the common name, element type, OSM selector and appropriate tags as a dict
searches = [{'object': 'guardrails', 'element_type': 'way', 'selector': '"barrier"="guard_rail"',
             'properties': {'FeatureType': 'VehicleBarrier', 'Subtype': 'WBeam', 'Source': source}},
            {'object': 'cable_barriers', 'element_type': 'way', 'selector': '"barrier"="cable_barrier"',
             'properties': {'FeatureType': 'VehicleBarrier', 'Subtype': 'Cable', 'Source': source}},
            {'object': 'concrete_barriers', 'element_type': 'way', 'selector': '"barrier"="jersey_barrier"',
             'properties': {'FeatureType': 'VehicleBarrier', 'Subtype': 'Concrete', 'Source': source}},
            {'object': 'sound_barriers', 'element_type': 'way', 'selector': '"wall"="noise_barrier"',
             'properties': {'FeatureType': 'Fence', 'Subtype': 'SoundBarrier', 'Source': source}},
            {'object': 'stop_signs', 'element_type': 'node', 'selector': '"highway"="stop"',
             'properties': {'FeatureType': 'Sign', 'Subtype': 'Regulatory', 'Device': 'Stop', 'Source': source}},
            {'object': 'yield_signs', 'element_type': 'node', 'selector': '"highway"="give_way"',
             'properties': {'FeatureType': 'Sign', 'Subtype': 'Regulatory', 'Device': 'Yield', 'Source': source}}
            ]

# clear the cache that OSMPythonTools makes
cache_path = './cache'
for f in os.listdir(cache_path):
    os.remove(os.path.join(cache_path, f))

# search for the area to get a OSM ID
nominatim = Nominatim()
nom_area = nominatim.query(area)

# note that this only returns the first area id and the search may yield many
area_id = nom_area.areaId()

# you can see the other results if you turn on verbosity
if verbose:
    print('\n' + 'Found the following places for the search: ' + area + '\n')
    area_json = nom_area.toJSON()
    print(json.dumps(area_json, indent=4, sort_keys=True))

# for each search query the database and save a new GEOJSON
for s in range(0, len(searches)):

    # build a query for the Overpass API
    query = overpassQueryBuilder(area=area_id, elementType=searches[s]['element_type'],
                                 selector=searches[s]['selector'], out='body', includeGeometry=True)

    # query Overpass
    overpass = Overpass()
    result = overpass.query(query, timeout=time_out)

    # you can see the results if you turn on verbosity
    if verbose:
        print('\n' + 'Found ' + str(result.countElements()) + ' elements of type '
              + searches[s]['element_type'] + ' with selector ' + searches[s]['selector'] + ':' + '\n')

    # combine the found tags and geometries to build GEOJSON entries
    geojson = {'type': 'FeatureCollection', 'features': []}
    for i in range(0, len(result.elements())):
        # setup the properties section, save the OSM tags in the Notes sections
        properties = searches[s]['properties']
        properties['Notes'] = json.dumps(result.elements()[i].tags(), indent=4)
        # setup the feature
        feature = {'type': 'Feature',
                   'properties': properties,
                   'geometry': result.elements()[i].geometry()
                   }
        # add it to the geojson
        geojson['features'].append(feature)

    # save the resulting data as a GEOJSON
    output_file = output_folder + searches[s]['object'] + '.geojson'
    with open(output_file, 'w', encoding='utf-8') as f:
        json.dump(geojson, f, ensure_ascii=False, indent=2)
