### OpenStreetMap

OpenStreetMap is the definitive community maintained map. It provides a wealth of datapoints for our road safety map 
and also informs the base map we plot our points on. 

[website](https://www.openstreetmap.org/about)

[copyright and license](https://www.openstreetmap.org/about)
