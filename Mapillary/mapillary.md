### Mapillary

Mapillary is a crowd sourced street view application which offers more than 1 billion images and a variety of AI 
identified objects to the public for free. Included in these detections are guardrail, fences, and barriers. 

[website](https://www.mapillary.com/app/)