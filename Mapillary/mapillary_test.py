import json
import requests
import argparse

# Setup the arg parse
parser = argparse.ArgumentParser(
    description="Returns Mapillary map features in a given bounding box. See "
                "https://www.mapillary.com/developer/api-documentation/#map-features for details.")
parser.add_argument("max_lat",
                    help="Latitude of the North West corned of the bounding box")
parser.add_argument("min_lng",
                    help="Longitude of the North West corned of the bounding box")
parser.add_argument("min_lat",
                    help="Latitude of the South East corned of the bounding box")
parser.add_argument("max_lng",
                    help="Longitude of the South East corned of the bounding box")
parser.add_argument("--feature_layer",
                    help="lines, points, or trafficsigns. See "
                         "https://www.mapillary.com/developer/api-documentation/#map-feature-layers",
                    default="lines")
parser.add_argument("--feature",
                    help="Type of feature. Accepts wildcards. See "
                         "https://www.mapillary.com/developer/api-documentation/#map-feature-layers",
                    default="*")
parser.add_argument("--output_json",
                    help="Path for the raw GeoJSON from Mapillary. Defaults to './output/output.geojson'.",
                    default="./output.geojson")
parser.add_argument("--output_csv",
                    help="Path for the formatted output csv. Defaults to './output/output.csv'.",
                    default='./output.csv')
parser.add_argument("--verbose",
                    help="Increase output detail.",
                    action="store_true")

def main():
    """ Parse arguments. Run the query. Save the result.
    """
    # parse args
    args = parser.parse_args()

    # set our building blocks
    min_lat = args.min_lat
    min_lng = args.min_lng
    max_lat = args.max_lat
    max_lng = args.max_lng
    feature_layer = args.feature_layer
    values = args.feature
    per_page = 200
    client_id = 'WUhncXZsWEhMZnZnWU1zeFdqYTRDZTpkODg2ODNiOTFmNDgxZGRm'  # client ID

    # print the parameters
    if args.verbose:
        print("Bounding Box: {}, {}, {}, {}\nFeature Layer: {}\nFeature(s): {}\n".format(min_lat, max_lng, max_lat, min_lng, feature_layer, values))

    # API call URL with sort_by=key which enables pagination, insert building blocks
    url = (
        'https://a.mapillary.com/v3/map_features?layers={}&bbox={},{},{},{}&per_page={}&client_id={}&values={'
        '}&sort_by=key').format(
        feature_layer, min_lng, min_lat, max_lng, max_lat, str(per_page), client_id, values)

    # create an empty GeoJSON to collect all the features we find
    output = {"type": "FeatureCollection", "features": []}

    with open(args.output_json, 'w') as outfile:  # set output filename

        # print the API call, so we can click it to preview the first response
        print("First response: " + url)

        # get the request with no timeout in case API is slow
        r = requests.get(url, timeout=None)

        # check if request failed, if failed, keep trying - 200 is a success
        while r.status_code != 200:
            r = requests.get(url, timeout=None)

        data = r.json()  # get a JSON format of the response
        data_length = len(data['features'])  # get a count of how many images
        count = 0
        for feature in data['features']:
            output['features'].append(feature)  # append each image to our empty geojson
            count = count + 1

        # if we receive the set number of items, the response was full and there should be a next page
        while data_length == per_page:

            # get the URL for a next page
            link = r.links['next']['url']

            # retrieve the next page in JSON format
            r = requests.get(link)

            # try again if the request fails
            while r.status_code != 200:
                r = requests.get(url, timeout=None)

            data = r.json()

            for feature in data['features']:
                output['features'].append(feature)

                count = count + 1

            print('Total features: {}'.format(len(output['features'])))  # print total count
            data_length = len(data['features'])  # update data length

        # send collected features to the local file
        json.dump(output, outfile)

        # Print the key of each object, its type, the number of detections and a link to each photo
        for o in output['features']:
            print('Key: {}, Type: {}'.format(o['properties']['key'], o['properties']['value']))
            for d in o['properties']['detections']:
                print('    Image: https://www.mapillary.com/map/im/{}'.format(d['image_key']))

    print('Found {} features'.format(data_length))  # once all images are pushed to a GeoJSON and saved, we finish
    print('DONE')

if __name__ == '__main__':
    main()
