### The National Highway Traffic Safety Administration (NHTSA)

NHTSA publishes a number of valuable data sets for studying the performance of roadside safety hardware. These include:
- The Fatal Accident Reporting System (FARS) [info](https://www.nhtsa.gov/research-data/fatality-analysis-reporting-system-fars) [download](https://www.nhtsa.gov/node/97996/251)
    + GPS locations of each crash are given in the `accident.sas7bdat` file in the `FARSXXXXNationalSAS` folder
- The Crash Report Sampling System (CRSS) [info](https://www.nhtsa.gov/crash-data-systems/crash-report-sampling-system) [download](https://www.nhtsa.gov/node/97996/221) 

Given the GPS coordinates of a crash one can assess that crash using a variety of tools:
- Google Streetview - paste the coordinates into the search bar on the [maps site](https://www.google.com/maps/) and try using the [time travel feature](https://blog.google/products/maps/go-back-in-time-with-street-view/)
- Mapillary - paste the coordinates in the search bar within the [app](https://www.mapillary.com/app/)