"""
Uses the sas7bdat library to convert .sas7bdat files to csvs via pandas

"""

# Imports ----------------------------------------------------------------------
from sas7bdat import SAS7BDAT
import time
import os
import argparse

# Input ------------------------------------------------------------------------
def main():
    """ Main program.

    Parses arguements, performs the copy
    """
    # Setup the arg parse
    parser = argparse.ArgumentParser(description="Converts a '.sas7bdat' "
                                                 + " into a '.csv'")
    parser.add_argument("input_file",
                        help="File path of input '.sas7dbat' file.")
    parser.add_argument("--output_file",
                        help="Output file path. Defaults to '[input_file].csv'.")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")

    # parse the args
    args = parser.parse_args()
    
    # time the operation - if verbose
    if args.verbose:
        start = time.time()

    # open the file
    if os.path.isfile(args.input_file):
        with SAS7BDAT(args.input_file) as reader:
            df = reader.to_data_frame()  # save it as a pandas dataframe
    else:
        print('Given input file does not exist. Please try again.')
        return

    # print some stats on the file - if verbose
    if args.verbose:
        print(df.dtypes)
        print(df)

    # determine the output file name
    if args.output_file:
        output_file = args.output_file
    else:
        base, ext = os.path.splitext(args.input_file)
        output_file = base + '.csv'

    # save the data frame to a csv
    df.to_csv(output_file)

    # report completion and time - if verbose
    if args.verbose:
        print("DONE - completed in " + str(time.time()-start) + " seconds")


if __name__ == '__main__':
    main()