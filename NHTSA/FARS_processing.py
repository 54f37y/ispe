"""
Converts the FARS data files into GEOJSON's ready for map.safe7y.com

"""

# Imports ----------------------------------------------------------------------
from sas7bdat import SAS7BDAT
import time
import pandas as pd
import os
import argparse
import json


# Settings ---------------------------------------------------------------------


# Input ------------------------------------------------------------------------
def main():
    """ Main program.

    Parses arguements, performs the copy
    """
    # Setup the arg parse
    parser = argparse.ArgumentParser(description="Converts a '.sas7bdat' "
                                                 + " into a '.csv'")
    parser.add_argument("input_file",
                        help="File path of input '.sas7dbat' file.")
    parser.add_argument("--output_file",
                        help="Output file path. Defaults to '[input_file].json'.")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")
    # parse the args
    args = parser.parse_args()

    # time the operation - if verbose
    if args.verbose:
        start = time.time()

    # convert the files from SAS7BDAT
    df = sas7bdat_to_df(args.input_file)

    # save only the columns we need
    # see the FARS Analytical User Manual for details https://www.nhtsa.gov/filebrowser/download/280641
    df = df[['DAY', 'MONTH', 'YEAR', 'LATITUDE', 'LONGITUD', 'FATALS', 'ROUTE']]

    # rename the lat and lon columns and drop unreported columns
    df['latitude'] = df['LATITUDE']
    df['longitude'] = df['LONGITUD']
    df = df.drop(columns=['LATITUDE', 'LONGITUD'])
    df = df[(df['longitude'] != 777.777) & (df['longitude'] != 888.888) & (df['longitude'] != 999.999)]

    # tell us about the rows without data
    if args.verbose:
        dropped = len(df[(df['longitude'] == 777.777) | (df['longitude'] == 888.888) | (df['longitude'] == 999.999)])
        print('Dropped ' + str(dropped) + ' entries with no location information')
        print(df)

    # convert the date to datetime
    # TODO: Confirm with NHTSA whether these dates are in UTC or the local timezone then add in the hour and minute
    df['time'] = df['MONTH'].map(int).map(str) + '/' + df['DAY'].map(int).map(str) + '/' + df['YEAR'].map(int).map(str)
    df['CrashDate'] = pd.to_datetime(df['time'], format='%m/%d/%Y')
    df = df.drop(columns=['DAY', 'MONTH', 'YEAR', 'time'])
    df['CrashDate'] = df['CrashDate'].map(str)

    # get the roadway details from the
    df['RoadType'] = df['ROUTE'].map(lambda x: parse_route(x))
    df = df.drop(columns=['ROUTE'])

    # TODO: Add notes to each of these based on the other details

    # add some map.safe7y.com specific data fields
    df['FeatureType'] = 'Crash'
    df['Subtype'] = 'Fatal'
    df['Source'] = 'NHTSA - FARS'
    df['WikiLink'] = 'https://wiki.safe7y.com/e/en/Features/Crash'
    df['TotalFatalities'] = df['FATALS']
    df = df.drop(columns=['FATALS'])

    # print some stats on the file - if verbose
    if args.verbose:
        print(df.dtypes)
        print(df)

    # determine the output file name
    if args.output_file:
        output_file = args.output_file
    else:
        base, ext = os.path.splitext(args.input_file)
        output_file = base + '.geojson'

    # save the data frame to a json
    # df.to_json(output_file, orient='index', indent=2)
    properties = ['CrashDate', 'RoadType', 'FeatureType', 'Subtype', 'Source', 'WikiLink', 'TotalFatalities']
    json_df = df_to_geojson(df, properties)

    # save the json
    with open(output_file, 'w', encoding='utf-8') as f:
        json.dump(json_df, f, ensure_ascii=False, indent=2)

    # report completion and time - if verbose
    if args.verbose:
        print("DONE - completed in " + str(time.time() - start) + " seconds")


def df_to_geojson(df, properties, lat='latitude', lon='longitude'):
    """ Converts a pandas dataframe to GEOJSON. Thanks to Geoff Boeing for this function.
        https://geoffboeing.com/2015/10/exporting-python-data-geojson/
        df -- dataframe to convert
        properties -- list of column name strings to convert into properties
        lat -- column name string of the Latitude column
        lon -- column name string of the Longitude column
    """
    geojson = {'type': 'FeatureCollection', 'features': []}
    for _, row in df.iterrows():
        feature = {'type': 'Feature',
                   'properties': {},
                   'geometry': {'type': 'Point',
                                'coordinates': []}}
        feature['geometry']['coordinates'] = [row[lon], row[lat]]
        for prop in properties:
            feature['properties'][prop] = row[prop]
        geojson['features'].append(feature)
    return geojson


def parse_route(route_num):
    """ Converts FARS ROUTE data into a human readable string

        See the FARS Analytical User Manual, ACCIDENT data file for details
        https://www.nhtsa.gov/filebrowser/download/280641

        route_num -- 0 - 9 integer identifying the route
    """
    if route_num == 1:
        return 'InterstateOrMotorway'
    elif 2 <= route_num <= 4:
        return 'HighwayOrTrunk'
    elif 5 <= route_num <= 9:
        return 'Street'
    else:
        return None


def sas7bdat_to_df(path):
    """ Converts a SAS7BDAT file to a pandas dataframe
        Returns a Pandas Dataframe
        Returns None if the given file does not exist
    """
    # open the file
    if os.path.isfile(path):
        with SAS7BDAT(path) as reader:
            df = reader.to_data_frame()  # save it as a pandas dataframe
    else:
        return None
    return df


if __name__ == '__main__':
    main()
