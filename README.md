# Street Sweeper
===

This is a collection of scripts to collect and process road safety related data from a variety of data sources.  

## General Process

1. Download the data
2. Explore the data
3. Refactor the data into map_safe7y format 
4. Upload the data to the map_saf7ey postGIS database

Each organization has it's own folder including details on the data available from them and a selection of scripts and Jupyter notebooks for extracting it. 

**Please Note:** Most of these scripts are designed to be run infrequently and many or all of them may be in various states of disrepair.

## Setup

The following simplified setup instructions are provided for Windows. To get started you'll need [git](https://git-scm.com/downloads) and [anaconda](https://www.anaconda.com/products/individual) and some basic command line experience. 

1. Clone the script from gitlab `git clone git@gitlab.com:54f37y/ispe.git`
2. Navigate to the project directory `cd ./ispe`
3. Create the virtual environment `conda env create -f enviroment.yml`
4. Launch the venv `conda active streetsweeper`
5. Set you enviroment variables by editing `sample.env` and saving it simply as `.env`

With luck, the scripts should now work perfectly 😉
