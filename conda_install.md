I'm trying to create a clean conda enviroment for the project so that I can use some of the features released in GeoPandas 0.10. This has proved suprisingly complex so I'm making a list of the commands I've used for installation. Hopefully I can get a decent enviorment.yml file out of this, but if not at least I can try again.

conda create -n streetsweeper2
conda install pandas
conda remove pandas
conda install -c conda-forge pandas
conda install -c conda-forge geopandas
conda install -c conda-forge jupyter
conda install -c conda-forge pygeos
pip uninstall rtree
conda install -c conda-forge sqlalchemy
pip install python-dotenv
conda install -c conda-forge psycopg2