from urllib import request
import json

# the short name of the organization, what it is known by in our datasets
org = 'TNDOT'

# If you want to add or change a data set link it needs to be updated below
links = {
        'guardrail': "https://opendata.arcgis.com/datasets/e6fc8455ea2b4774b98114025feccefe_0.geojson",
        'guardrail_terminals': 'https://opendata.arcgis.com/datasets/14ec6d50e63b49a986f45f2b16ab0550_0.geojson',
        'pavement_markings': 'https://opendata.arcgis.com/datasets/c183cdd44a474ec5981a598eca946a8f_0.geojson',
        'rumble_strip': 'https://opendata.arcgis.com/datasets/dbf60d13faf649ee9b538c9a060701fb_0.geojson',
        'mile_markers': 'https://opendata.arcgis.com/datasets/b51d9940cb7c4f6491c7b794c2037a64_0.geojson',
        'concrete_barrier_wall': 'https://opendata.arcgis.com/datasets/784fdeb5348c418fac9fa8a95b9d8e30_0.geojson',
        'flat_sheet_signs': 'https://opendata.arcgis.com/datasets/35e8ee994d234e09a9583bbaf553a5cf_0.geojson',
        'raised_pavement_markings': 'https://opendata.arcgis.com/datasets/3e12710c0c65456d97624884f940b602_0.geojson',
        'specialty_pavement_markings': 'https://opendata.arcgis.com/datasets/aff8d2f2d6774c0e871aa115a1262811_0.geojson',
    }

download_dir = 'C:\\Users\\aiden\\safe7y\\ispe\\data\\'

def download(key):
    # download the data from a given link in links -  ASSUMES JSON FORMAT
    filename = org + '_' + key
    filepath = download_dir + filename + '.json'
    with open(filepath, 'w') as file:
        data = request.urlopen(links[key]).read()
        json_data = json.loads(data.decode('utf-8'))
        file.write(json.dumps(json_data))
    return(filepath)