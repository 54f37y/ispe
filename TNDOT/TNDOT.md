# Tennessee DOT

## Data Sources

https://hub.arcgis.com/search?owner=TDOT_GIS
- Guardrail (lines) - https://opendata.arcgis.com/datasets/e6fc8455ea2b4774b98114025feccefe_0.geojson
- Guardrail Terminals (lines) - https://opendata.arcgis.com/datasets/14ec6d50e63b49a986f45f2b16ab0550_0.geojson
- Pavement Markings - https://opendata.arcgis.com/datasets/c183cdd44a474ec5981a598eca946a8f_0.geojson
- Rumble Strip - https://opendata.arcgis.com/datasets/dbf60d13faf649ee9b538c9a060701fb_0.geojson
- Mile Markers - https://opendata.arcgis.com/datasets/b51d9940cb7c4f6491c7b794c2037a64_0.geojson
- Concrete Barrier Wall - https://opendata.arcgis.com/datasets/784fdeb5348c418fac9fa8a95b9d8e30_0.geojson
- Flat Sheet Signs - https://opendata.arcgis.com/datasets/35e8ee994d234e09a9583bbaf553a5cf_0.geojson
- Raised Pavement Markings - https://opendata.arcgis.com/datasets/3e12710c0c65456d97624884f940b602_0.geojson
- Specialty Pavement Markings - https://opendata.arcgis.com/datasets/aff8d2f2d6774c0e871aa115a1262811_0.geojson

## Upload History

2021-09-23: Uploaded all guradrail lines using `TNDOT-Guardrail.ipynb`